const fs = require("fs");
const PDFDocument = require("pdfkit");
const { addLogo, addTitle, addHLine, addInfoCard, addTable, addTotalRow } = require("./utils/anzarPDFKitComponent")

const PDF_DIR_PATH = "./generated files"
const ASSETS_DIR_PATH = "./assets"
const FONTS_DIR_PATH = "./fonts"

class BDC_TEMPLATE_INFO {
    static bg_img_tl = ASSETS_DIR_PATH + "/" + "img-tl.png"
    static bg_img_bl = ASSETS_DIR_PATH + "/" + "img-bl.png"
    static bg_img_tr = ASSETS_DIR_PATH + "/" + "img-tr.png"
    static bg_img_br = ASSETS_DIR_PATH + "/" + "img-br.png"
    static logo_img = ASSETS_DIR_PATH + "/" + "logo.png"
    static r_font_slanted = FONTS_DIR_PATH + "/" + "Amiri-Slanted.ttf"
    static r_font_regular = FONTS_DIR_PATH + "/" + "Amiri-Regular.ttf"
    static r_font_Bold = FONTS_DIR_PATH + "/" + "Amiri-Bold.ttf"
    static l_font_regular = "Helvetica"
    static l_font_Bold = "Helvetica-Bold"
    static format = "A4"
    static margin  = 50
    static text_size = 1
    static width = 595
    static height = 841
    static header = { start : 0, end : 200 }
    static body = { start : 280, end : 600 }
    static footer = { start : 620, end : 841 }
}



const templates = {'BDC' : BDC_TEMPLATE_INFO}
const direction = {'AR' : 'R', 'FR' : 'L'}






class AnzarPdf {
    
    constructor(type, lang) {
        this.docObject = new PDFDocument({ size: "A4", margin: 50 });
        this.T_INFO = templates[type] || BDC_TEMPLATE_INFO
        this.T_DIRECT = direction[lang] || 'L'
    }

    generate(data, fileName) {
        this.drawBackGround()
        this.drawHeader(data.header)
        this.drawBody(data.body)
        this.drawFooter(data.header)
        this.createFile(fileName)
    }
    
    createFile(fileName) {
        this.docObject.end();
        this.docObject.pipe(fs.createWriteStream(PDF_DIR_PATH + "/" + fileName));
    }

    drawBackGround() {
        if (this.T_DIRECT === 'L')
        {
            // Left
            this.docObject
            .image(
                this.T_INFO.bg_img_tl,
                0,
                0,
                { width : this.T_INFO.width}
            )
            .image(
                this.T_INFO.bg_img_bl,
                0,
                this.T_INFO.footer.start,
                { width : this.T_INFO.width}
            )

        }
        else
        {
            // Right
            this.docObject
            .image(
                this.T_INFO.bg_img_tr,
                0,
                0,
                { width : this.T_INFO.width}
            )
            .image(
                this.T_INFO.bg_img_br,
                0,
                this.T_INFO.footer.start,
                { width : this.T_INFO.width}
            )
        }
    }

    drawHeader(headerData) {
        let tracker = this.T_INFO.header.start

        tracker += 20
        if (this.T_DIRECT === 'L') // LEFT
        {
            addLogo(this.docObject, this.T_INFO.margin, this.T_INFO.logo_img, tracker, 'center', 1)
            tracker += 75
            addTitle(this.docObject, this.T_INFO.margin, "Bon De Chargement", tracker, 'center', 1.4, this.T_INFO.l_font_Bold, this.T_DIRECT)
            tracker += 18
            addHLine(this.docObject, this.T_INFO.margin, tracker, 'center', 500)
            tracker += 25
            const keyVal_y_Spacing = 3
            addInfoCard(this.docObject, this.T_INFO.margin, headerData, tracker, 'left', keyVal_y_Spacing, 0.8, this.T_INFO.l_font_Bold, this.T_INFO.l_font_regular, false)
        }
        else // RIGHT
        {
            addLogo(this.docObject, this.T_INFO.margin, this.T_INFO.logo_img, tracker, 'center', 1)
            tracker += 75
            addTitle(this.docObject, this.T_INFO.margin, "Bon De Chargement", tracker, 'center', 1.4, this.T_INFO.l_font_Bold, this.T_DIRECT)
            tracker += 18
            addHLine(this.docObject, this.T_INFO.margin, tracker, 'center', 500)
            tracker += 25
            const keyVal_y_Spacing = 3
            addInfoCard(this.docObject, this.T_INFO.margin, headerData, tracker, 'right', keyVal_y_Spacing, 0.8, this.T_INFO.r_font_Bold, this.T_INFO.r_font_regular, true)

        }
    }
    drawBody(bodyData) {
        let tracker = this.T_INFO.body.start
        if (this.T_DIRECT === 'L') // LEFT
        {
            addTable(this.docObject, this.T_INFO.margin, bodyData, tracker, "center", 480, this.T_INFO.l_font_Bold, this.T_INFO.l_font_regular, 0.8, 0.6, false)
        }
        else
        {
            addTable(this.docObject, this.T_INFO.margin, bodyData, tracker, "center", 480, this.T_INFO.r_font_Bold, this.T_INFO.r_font_regular, 0.8, 0.6, true)
        }
        // addTable(this.docObject, this.T_INFO.margin, bodyData, tracker, "center", 500, this.T_INFO.r_font_Bold, this.T_INFO.r_font_regular, 0.8, 0.6, this.T_DIRECT)
        // doc, margin, list_of_values, y, position, width, total_font, total_size, rlt=false
        //addRow(this.docObject, list_of_titles, position, font, size)
    }
    drawFooter(dataHeader) {
        let tracker = this.T_INFO.footer.start + 50
        if (this.T_DIRECT === 'L') // LEFT
        {
            addTotalRow(this.docObject, this.T_INFO.margin,["", "", "","", "TOTAL", dataHeader["Total"]],  tracker, "center", 480, this.T_INFO.l_font_Bold, 0.8,false)
        }
        else
        {
            addTotalRow(this.docObject, this.T_INFO.margin,[dataHeader["المجموع"], "المجموع","", "","",  ""],  tracker, "center", 480, this.T_INFO.r_font_Bold, 0.8,true)
        }

    }

}


module.exports = { AnzarPdf }