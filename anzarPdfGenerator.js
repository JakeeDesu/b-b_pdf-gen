const { AnzarPdf } = require('./anzarPdf_template.js')
const fs = require("fs");


const pdf_file_name = "test.pdf"
const pdf_file_name_ar = "test.pdf"



 // READ DATA
const data = fs.readFileSync('./mockups/dummy_data_chargement_unique_driver.json');
const objectified_data = JSON.parse(data)

 // READ DATA
 const ar_data = fs.readFileSync('./mockups/dummy_data_chargement_unique_driver-ar.json');
 const ar_objectified_data = JSON.parse(ar_data)

// CREATE TEMPLATE FOR ANZAR PDF
let new_pdf = new AnzarPdf('BDC', 'FR')
// let new_pdf = new AnzarPdf('BDC', 'AR')

// RENDER DATA AND GENERATE PDF
new_pdf.generate(objectified_data.data, pdf_file_name)
// new_pdf.generate(ar_objectified_data.data, pdf_file_name)