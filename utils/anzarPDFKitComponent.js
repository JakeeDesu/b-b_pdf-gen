
const { text } = require("pdfkit");
const PDFDocument = require("pdfkit");

const getXPosition = (block_cords, elem_Width, position) => {
    const half_elem_width = elem_Width / 2
    const block_width = block_cords.end - block_cords.start

    const xPositions = {
        'left' : block_cords.start,
        'center' : block_cords.start + ((block_width / 2) - half_elem_width),
        'right' : block_cords.end - elem_Width,
    }

    return xPositions[position] || xPositions["left"]
}

const Logo  = (doc, margin, img, y, position, size) => {
    
    const width  = 70 * size
    // const margin = 50
    const block = { start : 0 + margin, end : 595.28 - margin}

    doc.image(
        img,
        getXPosition(block, width, position),
        y,
        { width : width}
    )
}

const Title = (doc, margin, text, y, position, size, font, l_or_right) => {
    
    const font_size  = 15 * size
    const text_width =  595.28 /2
    // const margin = 0
    const block = { start : 0 + margin, end : 595.28 - margin}

    doc
    .font(font)
    .fontSize(font_size)
    .text(
        text,
        getXPosition(block, text_width, position),
        y,
        { width : text_width, baseline: "middle",  align: position }
    )
}

const HLine = (doc, margin, y, position, width) => {
    
    // const margin = 50
    const block = { start : 0 + margin, end : 595.28 - margin}


    doc
    .strokeColor("#D9D9D9")
    .lineWidth(1.5)
    .moveTo(getXPosition(block, width, position), y)
    .lineTo(getXPosition(block, width, position) + width, y)
    .stroke();

}
const WLine = (doc, margin, y, x_position, height) => {
    
    // const margin = 50
    const block = { start : 0 + margin, end : 595.28 - margin}


    doc
    .strokeColor("#aaaaaa")
    .lineWidth(1.5)
    .moveTo(x_position, y)
    .lineTo(x_position + height, y)
    .stroke();

}

const addKeyVal = (doc, margin, key_text, val_text, position, y, keyVal_position_x, spacing_between_keyVal, size, key_font, val_font, l_or_right) => {
    
    const font_size  = 15 * size
    const block = { start : 0 + margin, end : 595.28 - margin}
    let text_options = {}
    
    text_options["features"] = l_or_right ? ['rtla'] : []
    
    let keyVal_width = 0
    console.log("-----", key_font, font_size, key_text + " :", keyVal_position_x, y, {  baseline: "middle", align : position })
    // draw key
    if (l_or_right)
    {
        let key_posi = keyVal_position_x
        key_posi -= doc.font(key_font).fontSize(font_size).widthOfString(key_text, text_options)
        doc
        .font(key_font)
        .fontSize(font_size)
        .text(
            key_text,
            key_posi,                
            y,
            {baseline: "alphabetic", lineBreak : "false", features : l_or_right ? ['rtla'] : []}
        )
        let dr_posi = key_posi - doc.widthOfString(":", text_options) - spacing_between_keyVal
        doc
        .font(key_font)
        .fontSize(font_size)
        .text(
            ":",
            dr_posi,
            y,
            {baseline: "alphabetic", lineBreak : "false", features : l_or_right ? ['rtla'] : []}
        )
        let val_posi = dr_posi - doc.font(val_font).fontSize(font_size * 0.6).widthOfString(`${val_text}`, text_options) - spacing_between_keyVal
        
        
        // console.log("last position : ", val_posi)
        // // draw val
        doc
        .font(val_font)
        .fontSize(font_size * 0.6)
        .text(
            val_text,
            val_posi,
            y,
            {baseline: "alphabetic", lineBreak : "false", features : l_or_right ? ['rtla'] : []}
        )
    }
    else
    {
        doc
        .font(key_font)
        .fontSize(font_size)
        .text(
            key_text,
            keyVal_position_x,
            y,
            {baseline: "alphabetic", lineBreak : "false", features : l_or_right ? ['rtla'] : []}
        )
        keyVal_width = doc.widthOfString(key_text)
        doc
        .font(key_font)
        .fontSize(font_size)
        .text(
            ":",
            keyVal_position_x + keyVal_width + spacing_between_keyVal,
            y,
            {baseline: "alphabetic", lineBreak : "false", features : l_or_right ? ['rtla'] : []}
        )
        keyVal_width += doc.widthOfString(":") + spacing_between_keyVal
    
        // draw val
        let val_posi = keyVal_position_x + keyVal_width + spacing_between_keyVal 
        doc
        .font(val_font)
        .fontSize(font_size * 0.7)
        .text(
            val_text,
            val_posi,
            y,
            {baseline: "alphabetic", lineBreak : "false", features : l_or_right ? ['rtla'] : []}
        )

    }
}

const getKeyValSize = (newDoc, key_text, val_text, spacing_between_keyVal, size, key_font, val_font, rtl=false) => {
    const font_size  = 15 * size
    
    let keyVal_width = 0
    let keyVal_height = 0
    let text_options = {}

    text_options["features"] = rtl ? ['rtla'] : []
    newDoc
    .font(key_font)
    .fontSize(font_size)
    .text(
        key_text 
        ,
        text_options
    )
    newDoc
    .font(key_font)
    .fontSize(font_size)
    .text(
        ":" 
        ,
        text_options
    )
    keyVal_width = newDoc.widthOfString(key_text)
    keyVal_width += newDoc.widthOfString(":") + spacing_between_keyVal
    keyVal_height = newDoc.heightOfString(key_text)

    newDoc
    .font(val_font)
    .fontSize(font_size * 0.8)
    .text(
        val_text,
        text_options
    )
    keyVal_width += newDoc.widthOfString(val_text) + spacing_between_keyVal

    return {keyVal_width, keyVal_height}
}
const getCardSize = (cardObject, spacing_between_keyVal, size, key_font, val_font ) => {
    let newDoc = new PDFDocument({ size: "A4", margin: 50 })

    let maxWidth = 0
    let maxHeight = 0

    for (const elem_key of Object.keys(cardObject))
    {
        let {keyVal_width, keyVal_height} = getKeyValSize(newDoc, elem_key, cardObject[elem_key], spacing_between_keyVal, size, key_font, val_font )
        maxWidth = (maxWidth < keyVal_width) ? keyVal_width : maxWidth
        maxHeight = (maxHeight < keyVal_height) ? keyVal_height : maxHeight
    }


    // newDoc.end()
    return {maxWidth, maxHeight}
}

const InfoCard = (doc, margin, data, y, position, y_Spacing, size, key_font, val_font, rtl=false) => {
    
    const block = { start : 0 + margin, end : 595.28 - margin}
        
    // Card infos 
    const show_card = false
    const padding = 0 // card padding
    const spacing_between_keyVal = 5
    const {maxWidth, maxHeight} = getCardSize(data, spacing_between_keyVal, size, key_font, val_font)
    const card_position_x = getXPosition(block, maxWidth, position)
    const card_position_y = y

    const card_width =  maxWidth + (2 * padding)
    const card_height =  (maxHeight * Object.keys(data).length) +  (y_Spacing * (Object.keys(data).length - 1)) +  (2 * padding)
    
    // draw card

    show_card && doc
    .rect(
        card_position_x ,
      card_position_y ,
      card_width ,
      card_height
    )
    .strokeColor("#cccccc")
    .fillColor("#dddddd")
    .lineWidth(1)
    .fillAndStroke()
    .fillColor('#000000')
    
    // row info > key value

    // draw rows
    let keyVal_position_y = card_position_y + padding + (maxHeight / 2)
    let keyVal_position_x = rtl ? card_position_x - padding + maxWidth : card_position_x + padding
    for (const elem_key of Object.keys(data))
    {
        addKeyVal(doc, margin, elem_key, data[elem_key], position, keyVal_position_y, keyVal_position_x, spacing_between_keyVal, size, key_font, val_font, rtl)
        keyVal_position_y += maxHeight + y_Spacing
    }

}

const getTextHeight = (newDoc, text, width, font, size) => {
    const font_size  = 12 * size

    let height = 0

    newDoc
    .font(font)
    .fontSize(font_size)
    .text(
       text,
        0,
        0,
        { width : width, align: "left" }
    )
    width = newDoc.widthOfString(text, { width : width,  })
    height = newDoc.heightOfString(text, { width : width,  })
    return {width, height}

}

const getTextSize = (newDoc, text, font, size) => {
    const font_size  = 12 * size
    let width = 0
    let height = 0

    newDoc
    .font(font)
    .fontSize(font_size)
    .text(
       text
    )
    width = newDoc.widthOfString(text)
    height = newDoc.heightOfString(text)
    return {width, height}

}
const getMaxHeightText = (list_of_elements, width, font, size) => {
    let newDoc = new PDFDocument({ size: "A4", margin: 50 })
    
    let maxHeight = 0

    for (text_elem of list_of_elements)
    {
        let {width : current_width, height : current_height} =  getTextHeight(newDoc, text_elem, width, font, size)
        maxHeight = (maxHeight < current_height) ? current_height : maxHeight
    }

    // newDoc.end()
    return maxHeight

}

const getMaxTextSize = (list_of_elements, font, size) => {
    let newDoc = new PDFDocument({ size: "A4", margin: 50 })
    
    let maxWidth = 0
    let maxHeight = 0

    for (text_elem of list_of_elements)
    {
        let {width, height} =  getTextSize(newDoc, text_elem, font, size)
        maxWidth = (maxWidth < width) ? width : maxWidth
        maxHeight = (maxHeight < height) ? height : maxHeight
    }
    console.log(maxWidth, " >-< ",maxHeight / 2)
    // newDoc.end()
    return {maxWidth, maxHeight}

}

const getCellsSizes = (width, fixed_size, cells_nb, margin=null) => {


    let sum = fixed_size.reduce((a, b) => a + b, 0)
    let new_unit = width / sum

    let cells_sizes = []
    for (var i = 0; i < fixed_size.length; i++)
    {
        cells_sizes.push(fixed_size[i] * new_unit)
    }
    console.log("calculated cell's sizes -", cells_sizes)
    return cells_sizes
}

const Row = (doc, margin, list_of_element, y, position, width, font, size, show_dh, maping=null, text_alignment, rtl=false) => {
    const block = { start : 0 + margin, end : 595.28 - margin}
    const font_size  = 12 * size
    let dh_margin = 5
    const row_position_x = getXPosition(block, width, position)
    const row_position_y = y

    const cell_width = width / list_of_element.length
    const cells_sizes = getCellsSizes(width, maping)
    let maxCellHeight = getMaxHeightText(list_of_element, cell_width, font, size)

    if (rtl)
    {
        let cell_position_x = row_position_x
        let cell_position_y = row_position_y
        console.log("text_elem ---------->>>>> ", text_alignment)
        
        for (const [idx, text_elem] of list_of_element.entries())
        {
            let text_options = { width : cells_sizes[idx], height : maxCellHeight,  align: (idx != 0) ? text_alignment : "left", ellipsis : true}
            text_options["features"] = rtl ? ['rtla'] : []
            if (idx == 0 && show_dh)
            {
                doc
                .font(font)
                .fontSize(font_size)
                .text(
                    "دم",
                    cell_position_x,
                    cell_position_y,
                    text_options
                )

                // cell_position_x += doc.widthOfString("دم", text_options) +  dh_margin
            }
            doc
            .font(font)
            .fontSize(font_size)
            .text(
                (idx != 0 || !show_dh) ? text_elem : Number(text_elem).toFixed(2) ,
                (idx == 0 && show_dh) ? cell_position_x + doc.widthOfString("دم", text_options) +  dh_margin : cell_position_x,
                cell_position_y,
                text_options
            )
            cell_position_x += cells_sizes[idx]
        }
    }
    else
    {
        let cell_position_x = row_position_x
        let cell_position_y = row_position_y
        console.log("text_elem ---------->>>>> ", text_alignment)
        
        for (const [idx, text_elem] of list_of_element.entries())
        {
            let text_options = { width : cells_sizes[idx], height : maxCellHeight,  align: (idx != list_of_element.length - 1) ? text_alignment : "right" , ellipsis : true}
            text_options["features"] = rtl ? ['rtla'] : []
            doc
            .font(font)
            .fontSize(font_size)
            .text(
                (idx != list_of_element.length - 1 || !show_dh) ? text_elem : Number(text_elem).toFixed(2) + " dh",
                cell_position_x,
                cell_position_y,
                text_options
            )
            cell_position_x += cells_sizes[idx]
        }

    }
}

const Table = (doc, margin,list_of_objects, y, position, width, titles_font, values_font, titles_size, values_size, rlt=false) => {
    
    const block = { start : 0 + margin, end : 595.28 - margin}
    const columnSize = [10, 10, 20, 7, 8, 10]
    let columnSizeMapping = rlt ? columnSize.reverse() : columnSize
    let text_alignment = rlt ? "right" : "left"
    const row_y_spacing = 10
    const max_rows = 15
    let y_tracker = y

    const titles = rlt ? Object.keys(list_of_objects[0]).reverse() : Object.keys(list_of_objects[0])

    // draw table fields
    // let {maxWidth, maxHeight} = getMaxTextSize(titles, titles_font, titles_size)
    // let maxHeight = getMaxHeightText(titles, width / titles.length, titles_font, titles_size)
    let maxCH0 = 8

    // HLine(doc, margin, y_tracker , position, width)

    y_tracker +=  row_y_spacing
    Row(doc, margin, titles, y_tracker, position, width, titles_font, titles_size, false, columnSizeMapping, text_alignment, rlt)

    y_tracker += ( maxCH0 ) +  row_y_spacing
    HLine(doc, margin, y_tracker, position, width)
//     // // draw table content
    // let {maxCellWidth, maxCellHeight} = getMaxTextSize(titles, values_font, values_size)
    let next_row_position =  y_tracker + row_y_spacing
    let maxCH1 = 20

    for (let j = 0; j < list_of_objects.length && j < max_rows; j++)
    {
        let values = rlt ? Object.values(list_of_objects[j]).reverse() : Object.values(list_of_objects[j])
    
        // let maxCellHeight = getMaxHeightText(values, width / values.length, values_font, values_size)

        Row(doc, margin, values, next_row_position, position, width, values_font, values_size, true, columnSizeMapping, text_alignment, rlt)

        next_row_position += maxCH1// + row_y_spacing
        // next_row_position += ( maxCellHeight) + row_y_spacing
        // HLine(doc, margin, next_row_position, position, width)
        
        // next_row_position += row_y_spacing

    }
    // Row(doc, margin, ["", "", "","", "TOTAL", 3002], next_row_position, position, width, titles_font, titles_size, true, columnSizeMapping, text_alignment, rlt)

}

const addTotalRow = (doc, margin, list_of_values, y, position, width, total_font, total_size, rlt=false) => {
    const columnSize = [10, 10, 20, 7, 8, 10]
    let columnSizeMapping = rlt ? columnSize.reverse() : columnSize
    let text_alignment = rlt ? "right" : "left"

    Row(doc, margin, list_of_values, y, position, width, total_font, total_size, true, columnSizeMapping, text_alignment, rlt)
}
module.exports = {
    addLogo : Logo,
    addTitle : Title,
    addHLine : HLine,
    addInfoCard : InfoCard,
    addTable : Table,
    addTotalRow : addTotalRow
}